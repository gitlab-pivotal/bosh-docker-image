FROM ruby:2.1
RUN apt-get update -y && apt-get install -y git-core git s3cmd ;\
    gem install bosh_cli ;\
    wget https://s3-us-west-1.amazonaws.com/cf-cli-releases/releases/v6.20.0/cf-cli-installer_6.20.0_x86-64.deb ;\
    dpkg -i cf-cli-installer_6.20.0_x86-64.deb && apt-get install -f ;\
